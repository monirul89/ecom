

from django.urls import path
from . import views

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('addItemToCart/', views.addItemToCart, name='addItemToCart'),
    path('addToCartQtyIncrement/', views.addToCartQtyIncrement, name='addToCartQtyIncrement'),
    path('cart/', views.cartpage, name='cartpage'),
    path('products/', views.products, name='products'),
]
