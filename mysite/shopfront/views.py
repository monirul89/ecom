from email import message
from multiprocessing import context
from urllib import response
from django.db.models import Sum,Max,Min,Count,Q,F
from django.shortcuts import render
from shoppanel import models
from django.http import JsonResponse

import random
from random import randint

# Create your views here.

def homepage(request):

     mysession = random.random()     
     if "session" not in request.session:
          request.session['session'] = str(mysession)

     if request.method == "GET":
          sliders        = models.SliderInfo.objects.filter(status=True)
          categories     = models.Category.objects.filter(status=True)
          products       = models.ProductList.objects.filter(status=True)
          
          added_item = models.AddToCart.objects.filter(session_key=request.session.get('session')).count()

          context = {
               'sliders': sliders,
               'categories': categories,
               'products': products,
               'added_item' : added_item
          }
          return render(request, 'home/index.html', context)


def addItemToCart(request):
     product_id = request.POST.get('product_id')
     get_product = models.ProductList.objects.filter(id=product_id).first()

     chk_item = models.AddToCart.objects.filter(product_name_id = product_id,session_key = request.session.get('session'))
     if not chk_item:
          models.AddToCart.objects.create(
               product_name_id     = product_id,
               sale_price          = get_product.unit_price,
               total_price         = get_product.unit_price,
               session_key         = request.session['session']
          )
          success = 'succeefull'
          return JsonResponse(success, safe=False)
     else:
          success = 'Already exist'
          return JsonResponse(success, safe=False)

def cartpage(request):
     mysession = random.random()  
     if "session" not in request.session:
          request.session['session'] = str(mysession)

     product_id = request.POST.get('product_id')
     added_item = models.AddToCart.objects.filter(session_key=request.session.get('session')).count()
     get_product = models.AddToCart.objects.filter(session_key=request.session.get('session'))

     context = {
          'cart_products' :get_product,
          'added_item' : added_item,
     }
     return render(request, 'cart/cart.html', context)

def products(request):

     return render(request, 'products/index.html')

def addToCartQtyIncrement(request):

     product_id = request.POST.get('product_id')
     status = request.POST.get('status')
     
     if status == 'plus':
          models.AddToCart.objects.filter(product_name_id=product_id).update(
              quantity =  F('quantity')+1,
              total_price = F('sale_price')*F('quantity')
          )
          message = 'Item Incriment'
     else:
          models.AddToCart.objects.filter(product_name_id=product_id).update(
              quantity =  F('quantity')-1,
              total_price = F('sale_price')*F('quantity')
          )
          message = 'Item decriment'

     return JsonResponse(message, safe=False)