from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('superadmin/', admin.site.urls),
    path('', include('shoppanel.urls')),
    # path('', include('shopfront.urls')),
    path('api/', include('api.urls')),
]
