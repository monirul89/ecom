from django.urls import path
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('menus_list/', views.menus_list.as_view(), name='menus_list'),
]


urlpatterns = format_suffix_patterns(urlpatterns)