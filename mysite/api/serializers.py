
# from myapp.models import *
from rest_framework import serializers
from shoppanel.models import MenuList


class MenuListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuList
        fields = ['menu_name', 'menu_url', 'module_name']
