from django.shortcuts import render
from requests import Response
from rest_framework.views import APIView
from shoppanel import models
from api.serializers import MenuListSerializer
# Create your views here.

class menus_list(APIView):
     def get(self, request, format=None):
        menus_list            = models.MenuList.objects.all()
        serializer_data  = MenuListSerializer(menus_list, many=True)

        return Response(serializer_data)
