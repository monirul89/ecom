from django.apps import AppConfig


class ShoppanelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shoppanel'
