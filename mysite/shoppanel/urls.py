from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.register, name="register"),
    path('login/', views.userlogin, name="userlogin"),
    path('logout/', views.userlogout, name="userlogout"),
    path('forgot_password/', views.forgot_password, name="forgot_password"),
    path('forgot_password/reset_password/<str:slug>/', views.reset_password, name="reset_password"),

# Dashboard
    path('admin/', views.dashboard, name='dashboard'),

# Users
    path('admin/users/', views.user_list, name='user_list'),
    path('admin/user/edit/<int:id>/', views.user_edit, name='user_edit'),
    path('admin/user/view/<int:id>/', views.user_view, name='user_view'),

# Menu
    path('menus/', views.menus, name='menus'),
    path('menu/create/', views.menu_create, name='menu_create'),
    path('menu/taggle/<int:id>/', views.menu_taggle, name='menu_taggle'),
    path('menu/edit/<int:id>/', views.menu_edit, name='menu_edit'),
    path('menu/trash/<int:id>/', views.menu_trash, name='menu_trash'),

# Master Category       
    # path('mas_category/create/', views.create_mas_category, name='create_mas_category'),
    # path('mas_categories/', views.master_categories, name='master_categories'),
    # path('mas_category/edit/<int:id>/', views.mas_category_edit, name='mas_category_edit'),

# Category
    path('category/create/', views.create_category, name='create_category'),
    path('categories/', views.categories, name='categories'), 
    path('category/edit/<int:id>/', views.category_edit, name='category_edit'),
    path('category/taggle/<int:id>/', views.category_taggle, name='category_taggle'),
    path('category/trash/<int:id>/', views.category_trash, name='category_trash'),

# Sub Category
    path('sub_category/create/', views.create_sub_category, name='create_sub_category'),
    path('sub_categories/', views.sub_categories, name='sub_categories'),
    path('sub_category/taggle/<int:id>/', views.sub_category_taggle, name='sub_category_taggle'),
    path('sub_category/trash/<int:id>/', views.sub_category_trash, name='sub_category_trash'),
    path('sub_category/edit/<int:id>/', views.sub_categories_edit, name='sub_categories_edit'),




]
