
from django.contrib import admin
from . import models

class ShopProfileAdmin(admin.ModelAdmin):
    list_display = ['shop_name', 'email1', 'mobile1', 'address', 'status']
    list_filter = ['shop_name', 'mobile1', 'email1', 'status']
    search_fields = ['shop_name', 'email1','mobile1']

class SliderInfoAdmin(admin.ModelAdmin):
    list_display = ['slider_name','title1','upload_date','slider_order','photo','status']
    list_filter = ['slider_name','title1','status']
 

class ProductListAdmin(admin.ModelAdmin):
    list_display    = ['product_name_english', 'unit_price', 'quantity', 'photo', 'status' ]
    search_fields   = ['product_name_english', 'unit_price', 'quantity', 'status']
    list_per_page = 25
    date_hierarchy = 'created'
    ordering = ['created']

class ProductOffersAdmin(admin.ModelAdmin):
    list_display    = ['offer_title', 'insert_by', 'status' ]
    search_fields   =  ['offer_title', 'insert_by', 'status' ]
   
class DistrictInfoAdmin(admin.ModelAdmin):
    list_display  = ['district_name_bangla', 'district_name_english', 'ordering', 'add_date', 'status']
    list_filter  = ['district_name_bangla', 'district_name_english', 'ordering', 'add_date', 'status']
    search_fields = ['district_name_bangla', 'add_date']
    
    
class UpozillaInfoAdmin(admin.ModelAdmin):
    list_filter  = ['upozilla_name_bangla', 'upozilla_name_english', 'district_name', 'add_date', 'status']
    list_display  = ['upozilla_name_bangla', 'upozilla_name_english', 'district_name', 'add_date', 'status']
    search_fields = ['upozilla_name_bangla', 'district_name', 'add_date']


class PostOfficeInfoAdmin(admin.ModelAdmin):
    list_filter  = ['post_office_bangla', 'post_office_english', 'post_code', 'upozilla_name', 'add_date', 'status']
    list_display  = ['post_office_bangla', 'post_code', 'upozilla_name', 'add_date', 'status']
    search_fields = ['post_office_bangla', 'upozilla_name', 'add_date']
    
     
class UserRegistrationAdmin(admin.ModelAdmin):
    list_display  = ['full_name', 'user_type', 'email', 'mobile', 'designation', 'username', 'password', 'status']
    search_fields = ['full_name', 'email']     
     
class MenuListAdmin(admin.ModelAdmin):
    list_display  = ['menu_name', 'menu_url',  'module_order', 'status']
    search_fields = ['menu_name', 'module_types']
     
class UserAccessControlAdmin(admin.ModelAdmin):
    list_display  = ['menu', 'user', 'view_action', 'insert_action', 'update_action', 'delete_action', 'status']
    search_fields = ['menu', 'user']
     
class CourierServiceAdmin(admin.ModelAdmin):
    list_display  = ['courier_name', 'ordering', 'status']
    search_fields = ['courier_name', 'ordering']

class CategoryAdmin(admin.ModelAdmin):
    list_display  = ['category_name', 'category_ordering', 'created', 'photo', 'status']
    search_fields = ['category_name', 'category_ordering']
     
class SubCategoryAdmin(admin.ModelAdmin):
    list_display  = ['sub_category_name', 'category_name', 'sub_image', 'sub_details', 'created', 'status']
    search_fields = ['sub_category_name', 'category_name']

class ProductBrandAdmin(admin.ModelAdmin):
    list_display  = ['brand_name', 'details', 'ordering', 'created', 'status']
    search_fields = ['brand_name', 'details', 'ordering']

class ProductColorAdmin(admin.ModelAdmin):
    list_display  = ['color_name', 'color_code', 'status']
    search_fields = ['color_name', 'color_code', 'status']

class ProductSizeAdmin(admin.ModelAdmin):
    list_display  = ['size_name', 'ordering', 'status']
    search_fields = ['size_name', 'ordering', 'status']

class ProductPreviewImageAdmin(admin.ModelAdmin):
    list_display  = ['product_name', 'product_image', 'status']
    search_fields = ['product_name', 'product_image', 'status']
     
    
admin.site.register(models.ShopProfile, ShopProfileAdmin)
admin.site.register(models.SliderInfo,SliderInfoAdmin) 
admin.site.register(models.ProductList,ProductListAdmin)  
admin.site.register(models.ProductOffers,ProductOffersAdmin)  
admin.site.register(models.DistrictInfo, DistrictInfoAdmin)
admin.site.register(models.UpozillaInfo, UpozillaInfoAdmin)
admin.site.register(models.PostOfficeInfo, PostOfficeInfoAdmin) 
admin.site.register(models.UserRegistration, UserRegistrationAdmin) 
admin.site.register(models.MenuList, MenuListAdmin) 
admin.site.register(models.UserAccessControl, UserAccessControlAdmin) 
admin.site.register(models.CourierService, CourierServiceAdmin) 
admin.site.register(models.Category, CategoryAdmin) 
admin.site.register(models.SubCategory, SubCategoryAdmin) 
admin.site.register(models.ProductBrand, ProductBrandAdmin) 
admin.site.register(models.ProductColor, ProductColorAdmin) 
admin.site.register(models.ProductSize, ProductSizeAdmin) 
admin.site.register(models.ProductPreviewImage, ProductPreviewImageAdmin) 