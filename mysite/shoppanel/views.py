#hashlib for password hash
import hashlib
from multiprocessing import context
from urllib import request
from django.contrib.auth.models import AbstractUser
import os
#Login Required
from django.contrib.auth.decorators import login_required
#Import Model
from . import models
#redirect 
from django.shortcuts import redirect, render
# send_mail
from django.core.mail import send_mail
# Image save to local folder
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from django.db.models import Sum,Max,Min,Count,Q,F

#user model import
from django.contrib.auth import authenticate, get_user_model, logout, login

User = get_user_model()


#User List
def user_list(request):
    if request.method == "GET":
        data = models.UserRegistration.objects.all()
        user_photo = 'default_user.jpg'
        # print(data.id)
        context = {
            'users':data,
            'photo':user_photo
        }
        return render(request,'users/user_list.html', context)

#User Edit
def user_edit(request, id):
    if request.method == "GET":
        data = models.UserRegistration.objects.filter(id=id).first()
        user_photo = 'default_user.jpg'
        print(data.id)
        context = {
            'users':data,
            'photo':user_photo
        }
        return render(request,'users/user_edit.html', context)
    elif request.method == "POST":
          first_name     = request.POST.get('first_name')
          last_name      = request.POST.get('last_name')
          user_name      = request.POST.get('user_name')
          email          = request.POST.get('email')
          password       = request.POST.get('user_password')          
          terms_policy   = request.POST.get('terms_policy')
          is_staff       = request.POST.get('is_staff')

          result=User.objects.filter(id=id).update(username=user_name, first_name=first_name, last_name=last_name, email=email, is_staff=True, password=password, is_superuser=False, is_active=terms_policy)
          result.set_password(password) 
          result.save()
          if(result):
               return redirect('/admin')

          return redirect('/login/')

#User view
def user_view(request, id):
    if request.method == "GET":
        data = models.UserRegistration.objects.filter(id=id).first()
        user_photo = 'default_user.jpg'
        print(data.id)
        context = {
            'users':data,
            'photo':user_photo
        }
        return render(request,'users/user_view.html', context)

#User Registration
def register(request):
    if request.method == "GET":
          return render(request,'register/create.html')

    elif request.method == "POST":
          first_name     = request.POST.get('first_name')
          last_name      = request.POST.get('last_name')
          user_name      = request.POST.get('user_name')
          email          = request.POST.get('email')
          password       = request.POST.get('user_password')
          terms_policy   = request.POST.get('terms_policy')

          result=User.objects.create(username=user_name, first_name=first_name, last_name=last_name, email=email, is_staff=True, password=password, is_superuser=False, is_active=terms_policy)
          result.set_password(password) 
          result.save()
          if(result):
               return redirect('/admin')

          return redirect('/login/')


#User Login
def userlogin(request):   
     if request.method == "GET":
          return render(request,'register/login.html')

     elif request.method == "POST":
          user_name     = request.POST.get("user_name")
          password  = request.POST.get("user_password")

        #   if username and password and len(username) >= 3 and len(password) >= 6:
          if user_name and password:
               user = authenticate(username=user_name, password=password)
               print('User :',password)

          if user and user.is_staff==True: 
               login(request, user)
               return redirect('/admin')
          else:
               return redirect('/login/')

#User Logout
@login_required(login_url="/login")
def userlogout(request):
     logout(request)
     return redirect('/login/')

#Forgot Password
def forgot_password(request):
    if request.method == "GET":
        return render(request,'register/forgot_password.html')

    if request.method == "POST":
        email_box = str(request.POST.get('email'))
        check_email =User.objects.get(email=email_box).email
        
        print('check_email :', check_email)
        
        if check_email :
            request.session["email"] = str(email_box)
            # md5
            hascode  = hashlib.md5(email_box.encode())
            final_code = hascode.hexdigest()
            # end

            url = str(request.build_absolute_uri())
            url = url+'reset_password/'+final_code

            print('Url :', url)
            send_mail(
                'Your Password reset link', 'Please Click url : '+ url , 'monirul89@gmail.com', [check_email], fail_silently=False,
            )
            context ={
                'success':'Please check your email'
            }
            return render(request,'register/forgot_password.html', context)
        else:
            context ={
                'success':'Your Email Address Invalid'
            }
            return render(request,'register/forgot_password.html', context)

# Reset Password
def reset_password(request, slug):
    email = request.session["email"]
    hascode  = hashlib.md5(email.encode())
    final_code = hascode.hexdigest()

    if request.method == "GET":
        if final_code == slug:
            success = 'Your Email Verified'
            context ={
                'success':success
            }
            return render(request,'register/reset_password_confirm.html', context)

        elif final_code != slug:
            success = 'Invalid email address'
            context ={
                'success':success
            }
            return render(request,'register/forgot_password.html',context)
        
    elif request.method == "POST":
        
        newpassword = request.POST.get('password')
        password   = request.POST.get('confirm_password')

        if newpassword == password:
            if final_code == slug: 
                User.objects.filter(email=request.session["email"]).set_password(newpassword)
                success = 'Your Password has been Changed successfully'
                context ={
                    'success':success
                }
                return redirect('/login/')

            elif final_code != slug:
                success = 'Invalid email address'
                context ={
                    'success':success
                }
                return render(request,'register/forgot_password.html',context)
        else:
            context ={
                    'pasword':'Those passwords didn\'t match. Try Again'
                }
            return render(request,'register/reset_password_confirm.html',context)
            
# Dashboard or HOME
@login_required(login_url="/login") 
def dashboard(request):
     if request.method == "GET":
          return render(request, 'dashboard/index.html')

# Categories
@login_required(login_url="/login") 
def categories(request):
     if request.method == "GET":
          data = models.Category.objects.all()
          context = {
               'categories':data
          }
          return render(request, 'categories/categories.html', context)

# Master Category

# @login_required(login_url="/login")
# def create_mas_category(request):
#     if request.method == "GET":
#         data = models.Category.objects.all()
#         context = {
#             'categories':data
#         }
#         return render(request, 'categories/add_masCategory.html', context)

#     if request.method == "POST":
#         return redirect('/categories/')

# @login_required(login_url="/login")
# def master_categories(request):
#     if request.method == "GET":
#           data = models.Category.objects.all()
#           context = {
#                'categories':data
#           }
#           return render(request, 'categories/master_categories.html', context)

# Category Create
@login_required(login_url="/login")
def create_category(request):
    if request.method == "GET":
        data = models.Category.objects.all()
        context = {
            'categories':data
        }
        return render(request, 'categories/create_category.html', context)

    if request.method == "POST":
        category_name       = request.POST.get('category_name')
        category_details    = request.POST.get('category_details')
        chk_status  = True if request.POST.get('status') else False

        #Menu Image
        if bool(request.FILES.get('category_image', False)) == True:
            file = request.FILES['category_image']
            if not os.path.exists("shopfront/static/shopfront/media/category"):
                os.mkdir("shopfront/static/shopfront/media/category")

            category_image = default_storage.save("category/"+file.name, ContentFile(file.read()))
            
            if category_image:
                category_image = str("category/")+str(category_image).split("/")[-1]

            models.Category.objects.create(
                category_name   = category_name, 
                category_image   = category_image, 
                category_details = category_details, 
                status          = chk_status
            )
            return redirect('/categories/')

# Category Trash
@login_required(login_url="/login")
def category_trash(request, id):
    models.Category.objects.filter(id = id).delete()
    return redirect('/categories/')

# Category Edit
@login_required(login_url="/login")
def category_edit(request, id):
    if request.method=="GET":
        category_data = models.Category.objects.filter(id = id).first()
        context = {
            'category_data':category_data
        }
        return render(request, 'categories/edit_category.html', context)

    elif request.method=="POST": 

        name       = request.POST.get('category_name')
        details    = request.POST.get('category_details')
        chk_status  = True if request.POST.get('status') else False

        #Category Image
        if bool(request.FILES.get('category_image', False)) == True:
            file = request.FILES['category_image']
            if not os.path.exists("shopfront/static/shopfront/media/category"):
                os.mkdir("shopfront/static/shopfront/media/category")

            category_image = default_storage.save("category/"+file.name, ContentFile(file.read()))
            print('category_image :', category_image)
            if category_image:
                category_image = str("category/")+str(category_image).split("/")[-1]
                print('category_name :', name)
                models.Category.objects.filter(id=id).update(category_name = name, category_image = category_image, category_details = details, status = chk_status)

        return redirect('/categories/')

# Category Taggle
@login_required(login_url="/login") 
def category_taggle(request, id):   
    if request.method=="GET":
        data = models.Category.objects.get(id = id)
        if data.status == True:
            models.Category.objects.filter(id=id).update(status = 0)
        elif data.status == 0:
            models.Category.objects.filter(id=id).update(status = 1)
        return redirect('/categories')

# Sub Category Create
@login_required(login_url="/login") 
def create_sub_category(request):
    if request.method == "GET":
        data = models.Category.objects.all()
        context = {
            'categories':data
        }
        return render(request, 'categories/add_subCategory.html', context)

    if request.method == "POST":
        category_name_id    = request.POST.get('category_name_id')
        sub_category_name   = request.POST.get('sub_category_name')
        sub_ordering        = request.POST.get('sub_ordering')
        chk_status          = True if request.POST.get('check_status') else False

        #Category Image
        if bool(request.FILES.get('category_image', False)) == True:
            file = request.FILES['category_image']
            if not os.path.exists("shopfront/static/shopfront/media/category"):
                os.mkdir("shopfront/static/shopfront/media/category")

            category_image = default_storage.save("category/"+file.name, ContentFile(file.read()))
            
            if category_image:
                category_image = str("category/")+str(category_image).split("/")[-1]

            models.SubCategory.objects.create(
                category_name_id    = category_name_id, 
                sub_category_name   = sub_category_name, 
                sub_image           = category_image, 
                sub_ordering        = sub_ordering, 
                status              = chk_status
            )
            return redirect('/sub_categories/')

# Sub Category Create
@login_required(login_url="/login") 
def sub_categories_edit(request, id):

    get_data = models.SubCategory.objects.filter(id=id).first()
    if request.method == "GET":
        data = models.Category.objects.all()
        context = {
            'categories':data,
            'data_id':get_data,
        }
        return render(request, 'categories/sub_category_edit.html', context)

    if request.method == "POST":
        category_name_id    = request.POST.get('category_name_id')
        sub_category_name   = request.POST.get('sub_category_name')
        sub_ordering        = request.POST.get('sub_ordering')

        chk_status  = True if request.POST.get('check_status') else False
        print("chk_status:", chk_status)

        sub_image = str(get_data.sub_image) if get_data.sub_image else ""

        #Sub  Image
        if bool(request.FILES.get('sub_image', False)) == True:
            file = request.FILES['sub_image']
            if not os.path.exists("shopfront/static/shopfront/media/category"):
                os.mkdir("shopfront/static/shopfront/media/category")

            sub_image = default_storage.save("category/"+file.name, ContentFile(file.read()))
            
            if sub_image:
                sub_image = str("category/")+str(sub_image).split("/")[-1]

        models.SubCategory.objects.filter(id=id).update( category_name_id = category_name_id, sub_category_name= sub_category_name, sub_image = sub_image, sub_ordering = sub_ordering, status = chk_status)
        
        return redirect('/sub_categories/')

# SubCategory Delete
@login_required(login_url="/login")
def sub_category_trash(request, id):
    models.SubCategory.objects.filter(id = id).delete()
    return redirect('/sub_categories/')

# Sub Category switch
@login_required(login_url="/login") 
def sub_category_taggle(request, id):   
    if request.method=="GET":
        data = models.SubCategory.objects.get(id = id)
        if data.status == True:
            models.SubCategory.objects.filter(id=id).update(status = 0)
        elif data.status == 0:
            models.SubCategory.objects.filter(id=id).update(status = 1)

        return redirect('/sub_categories/')

# SubCategory 
@login_required(login_url="/login") 
def sub_categories(request):
    if request.method == "GET":
          subcategories = models.SubCategory.objects.all()
          context = {
               'subcategories':subcategories,
          }
          return render(request, 'categories/sub_categories.html', context)



# Menu 
@login_required(login_url="/login") 
def menus(request):
    if request.method == "GET":
        data = models.MenuList.objects.all()
        context = {
            'menu_list':data
        }
        return render(request, 'menus/index.html', context)

# Menu Create
@login_required(login_url="/login") 
def menu_create(request):     
    if request.method=="GET":
        return render(request,'menus/create.html')

    elif request.method=="POST":
        menu_name   = request.POST.get('menu_name')
        #Make slug use menu name
        menu_slug   = request.POST.get('menu_name').strip()
        slug_lower  = menu_slug.lower()
        use_hipen   = (slug_lower.replace(" ", "-"))
        slug        = use_hipen
        #End
        menu_order  = request.POST.get('menu_order')
        parent_id  = request.POST.get('is_root_id')
        is_active = request.POST.get('is_active')
        if request.POST.get('is_root') == 'on':
            is_root = 1
        else:
            is_root = 0
            
        #Menu Image
        if bool(request.FILES.get('menu_photo', False)) == True:
            file = request.FILES['menu_photo']
            if not os.path.exists("shopfront/static/shopfront/media/products"):
                os.mkdir("shopfront/static/shopfront/media/products")

            menu_images = default_storage.save("products/"+file.name, ContentFile(file.read()))
            
            if menu_images:
                menu_images = str("products/")+str(menu_images).split("/")[-1]
                print(menu_images)

            models.MenuList.objects.create(
                menu_name     = menu_name, 
                menu_url      = slug, 
                menu_photo    = menu_images, 
                menu_order    = menu_order, 
                is_root       = is_root, 
                parent_id     = parent_id,
                status        = is_active
            )
            return redirect('/menus')

        elif bool(request.FILES.get('menu_photo', False)) == False:
            models.MenuList.objects.create(
                menu_name     = menu_name, 
                menu_url      = slug, 
                menu_photo    = menu_images, 
                menu_order    = menu_order, 
                is_root       = is_root, 
                parent_id     = parent_id,
                status        = is_active
            )
            return redirect('/menus')

# Menu Taggle
@login_required(login_url="/login") 
def menu_taggle(request, id):   
    if request.method=="GET":
        data = models.MenuList.objects.get(id = id)
        if data.status == True:
            models.MenuList.objects.filter(id=id).update(status = 0)
        elif data.status == 0:
            models.MenuList.objects.filter(id=id).update(status = 1)

        return redirect('/menus')

# Menu Edit
@login_required(login_url="/login") 
def menu_edit(request, id):
    if request.method=="GET":
        data = models.MenuList.objects.filter(id=id).first()
        context ={
            'data':data
        }
        return render(request, 'menus/edit.html', context)
    elif request.method=="POST":
        menu_name   = request.POST.get('menu_name')
        #Make slug use menu name
        menu_slug   = request.POST.get('menu_name').strip()
        slug_lower  = menu_slug.lower()
        use_hipen   = (slug_lower.replace(" ", "-"))
        slug        = use_hipen
        #End
        menu_order  = request.POST.get('menu_order')
        parent_id  = request.POST.get('is_root_id')
        is_active = request.POST.get('is_active')
        if request.POST.get('is_root') == 'on':
            is_root = 1
        else:
            is_root = 0
            
        #Menu Image
        if bool(request.FILES.get('menu_photo', False)) == True:
            file = request.FILES['menu_photo']
            if not os.path.exists("shopfront/static/shopfront/media/products"):
                os.mkdir("shopfront/static/shopfront/media/products")

            menu_images = default_storage.save("products/"+file.name, ContentFile(file.read()))
            
            if menu_images:
                menu_images = str("products/")+str(menu_images).split("/")[-1]
                print(menu_images)

            models.MenuList.objects.filter(id=id).update(
                menu_name     = menu_name, 
                menu_url      = slug, 
                menu_photo    = menu_images, 
                menu_order    = menu_order, 
                is_root       = is_root, 
                parent_id     = parent_id,
                status        = is_active
            )
            return redirect('/menus')

        elif bool(request.FILES.get('menu_photo', False)) == False:
            models.MenuList.objects.filter(id=id).update(
                menu_name     = menu_name, 
                menu_url      = slug, 
                menu_order    = menu_order, 
                is_root       = is_root, 
                parent_id     = parent_id,
                status        = is_active
            )
            return redirect('/menus')

# Menu Trash
@login_required(login_url="/login") 
def menu_trash(request, id):   
    if request.method=="GET":
        models.MenuList.objects.filter(id=id).delete()

        return redirect('/menus')