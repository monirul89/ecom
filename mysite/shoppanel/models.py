import os
from socket import create_connection
from unicodedata import category
import django
from django.db import models
from django.utils.safestring import mark_safe
from ckeditor.fields import RichTextField
import datetime
from django.conf import settings
import re
from django.utils.timezone import get_fixed_timezone, utc
from shoppanel.views import User

# from grpc import Status

# #--------------Shop Profile Model--------------------#
class ShopProfile(models.Model):
    shop_name       = models.CharField(max_length=100, blank=True)
    slug            = models.CharField(max_length=100, blank=True)
    page_title      = models.CharField(max_length=100, blank=True)
    email1          = models.EmailField(max_length=100, blank=True)
    email2          = models.EmailField(max_length=100, blank=True)
    mobile1         = models.CharField(max_length=15,  blank=True)
    mobile2         = models.CharField(max_length=15,  blank=True)
    address         = models.TextField(blank=True)
    pro_details     = models.TextField(blank=True)
    logo            = models.ImageField(upload_to='images/logo')
    favicon_logo    = models.ImageField(upload_to='images/logo')
    about_content   = models.TextField(blank=True)
    about_images    = models.ImageField(upload_to='images/about_images')
    why_buy         = models.TextField(blank=True)
    map_locationo   = models.TextField(blank=True)
    facebook_link   = models.TextField(blank=True)
    instagram_link  = models.TextField(blank=True)
    youtube_link    = models.TextField(blank=True)
    linkedin_link   = models.TextField(blank=True)
    staring_year    = models.IntegerField()
    copy_right      = models.CharField(max_length=50, blank=True)
    status          = models.BooleanField(default=True)

    def __str__(self):
        return self.shop_name

    class Meta:
        db_table = 'shop_profile'
        verbose_name = 'Shop Profile'
        verbose_name_plural = 'Shop Profiles'

#--------------Slider Info Model--------------------#
class SliderInfo(models.Model):
    slider_name   = models.CharField(max_length=100, blank=True)
    title1        = models.CharField(max_length=200, blank=True)
    title2        = models.CharField(max_length=200, blank=True)
    slider_images = models.ImageField(upload_to='images/slider')
    slider_link   = models.TextField(blank=True)
    upload_date   = models.DateTimeField(auto_now_add=True)
    slider_order  = models.IntegerField(default=0)
    status        = models.BooleanField(default=True)

    class Meta:
        db_table = 'slider_list'
        verbose_name = 'Slider Image'
        verbose_name_plural = 'Slider Images'

    def url(self):
        return os.path.join('/static/shopfront/media/images/slider/', os.path.basename(str(self.slider_images)))

    def photo(self):
        return mark_safe('<img src = "{}" width="80"/>'.format(self.url()))

    def __str__(self):
        return self.slider_name
   
#-------------- Product Category --------------------#      
class Category(models.Model):   
    category_id         = models.IntegerField(blank=True, null=True) 
    category_name       = models.CharField(max_length=60, blank=True, null=True) 
    category_image      = models.ImageField(upload_to='images/category')
    category_details    = models.TextField(max_length=100, blank=True, null=True) 
    created             = models.DateTimeField(auto_now_add=True)  
    category_ordering   = models.IntegerField(default=0) 
    status              = models.BooleanField(default=True)
    
    class Meta:
        db_table = 'categories'
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def url(self):
        return os.path.join('/static/shopfront/media/images/category/', os.path.basename(str(self.category_image)))

    def photo(self):
        return mark_safe('<img src = "{}" width="80"/>'.format(self.url()))

    def __str__(self):
        return str(self.category_name)
    
#-------------- Mastar Sub Category --------------------#
class SubCategory(models.Model):  
    category_name        = models.ForeignKey(Category, on_delete=models.DO_NOTHING, null=True, blank=True) 
    sub_category_name    = models.CharField(max_length=100, blank=True, null=True)
    sub_image            = models.ImageField(upload_to='images/category', blank=True, null=True)
    sub_details          = models.TextField(max_length=200, blank=True, null=True) 
    created              = models.DateTimeField(auto_now_add=True)   
    sub_ordering         = models.IntegerField(default=False) 
    status               = models.BooleanField(default=True)

    def __str__(self):  # type: ignore
        return str(self.sub_category_name)

    class Meta:
        db_table = 'sub_category' 
        verbose_name = 'SubCategory'
        verbose_name_plural = 'SubCategories'

    def url(self):
        return os.path.join('/static/shopfront/media/images/category/', os.path.basename(str(self.sub_image)))

    def photo(self):
        return mark_safe('<img src = "{}" width="80"/>'.format(self.url()))

    def __str__(self):
        return str(self.sub_category_name)

#--------------Product Brand Model--------------------#
class ProductBrand(models.Model):  
    brand_name          = models.CharField(max_length=60, blank=True, null=True) 
    details             = models.TextField(blank=True, null=True)
    ordering            = models.IntegerField(default=0) 
    created             = models.DateTimeField(auto_now_add=True)   
    status              = models.BooleanField(default=True)

    def __str__(self):
        return str(self.brand_name)

    class Meta:
        db_table = 'product_brand_list' 

#--------------Product Size Model--------------------#
class ProductSize(models.Model):  
    size_name           = models.CharField(max_length=60, blank=True, null=True) 
    ordering            = models.IntegerField(default=0) 
    created             = models.DateTimeField(auto_now_add=True)   
    status              = models.BooleanField(default=True)

    def __str__(self):
        return str(self.size_name)

    class Meta:
        db_table = 'product_size_list' 

class ProductColor(models.Model):  
    color_name          = models.CharField(max_length=50, blank=True, null=True) 
    color_code          = models.CharField(max_length=50, blank=True, null=True) 
    status              = models.BooleanField(default=True)

    def __str__(self):
        return str(self.color_name)

    class Meta:
        db_table = 'product_color' 

#--------------Produc tList Model--------------------#
class ProductList(models.Model):    
    category                = models.ForeignKey(Category, on_delete=models.DO_NOTHING, null=True, blank=True)
    sub_category            = models.ForeignKey(SubCategory, on_delete=models.DO_NOTHING, null=True, blank=True)  
    Product_brand           = models.ForeignKey(ProductBrand, on_delete=models.DO_NOTHING, null=True, blank=True)  
    Product_color            = models.ForeignKey(ProductColor, on_delete=models.DO_NOTHING, null=True, blank=True)  
    Product_size            = models.ForeignKey(ProductSize, on_delete=models.DO_NOTHING, null=True, blank=True)  
    product_name_english    = models.CharField(max_length=250, blank=True, null = True)
    product_name_bangla     = models.CharField(max_length=250, blank=True, null = True) 
    slug                    = models.CharField(max_length=250, blank=True, null = True)
    thumb_image             = models.ImageField(upload_to='images/product_img')
    unit_price              = models.IntegerField(default=0) 
    purchase_discount       = models.CharField(max_length=15, default=0) # type: ignore
    sale_price              = models.CharField(max_length=15, blank=True, null = True)
    coupon_price            = models.IntegerField(default = 0, blank=True, null = True)
    discount                = models.CharField(max_length=15, default=0) # type: ignore
    quantity                = models.IntegerField(default=0) 
    origin                  = models.CharField(max_length=100, blank=True,null=True)  
    stock = (
        ('1', 'In Stock'), 
        ('2', 'Out of Stock'), 
    )
    sales_date              = models.DateField(auto_now_add=False)
    product_detail          = RichTextField(blank=True, null = True)
    is_pre_image            = models.BooleanField(default=0) # type: ignore
    created                 = models.DateField(auto_now_add=True)
    modified                = models.DateField(auto_now_add=True)
    created_by              = models.IntegerField(default=0)
    modified_by             = models.IntegerField(default=0) 
    status                  = models.BooleanField(default=1) # type: ignore

    def url(self):
        return os.path.join('/static/shopfront/media/images/product_img/', os.path.basename(str(self.product_image)))  # type: ignore
 
    def photo(self):
        return mark_safe('<img src = "{}" width="80" />'.format(self.url()))
  
    def __str__(self):
        return self.product_name_bangla

    class Meta:
        db_table = 'product_list'
        verbose_name = 'Product'
        verbose_name_plural = 'Product List'

class ProductWiseColor(models.Model):  
    product             = models.ForeignKey(ProductList, on_delete=models.DO_NOTHING, blank=True, null=True) 
    color               = models.ForeignKey(ProductColor, on_delete=models.DO_NOTHING, blank=True, null=True)  
    status              = models.BooleanField(default=True)

    def __str__(self):
        return str(self.product)

    class Meta:
        db_table = 'product_wise_color'        

#--------------AddTo Cart Model--------------------#
class AddToCart(models.Model):
    product_name   = models.ForeignKey(ProductList, on_delete=models.DO_NOTHING, null=True) 
    sale_price  = models.IntegerField(default = 0)
    total_price  = models.IntegerField(default = 0)
    discount    = models.IntegerField(default = 0)
    quantity    = models.IntegerField(default = 1)
    created    = models.DateTimeField(auto_now_add = True)
    session_key = models.CharField(max_length=230)

    def __str__(self):
        return str(self.product_name)
    
    class Meta:
        db_table = 'add_to_cart'
        verbose_name = 'AddTo Cart'
        verbose_name_plural = 'AddToCart List'

class ProductPreviewImage(models.Model):
    product_name  = models.ForeignKey(ProductList, on_delete=models.DO_NOTHING, null=True) 
    product_image = models.ImageField(upload_to='images/products')
    upload_date   = models.DateTimeField(auto_now_add=True)
    status        = models.BooleanField(default=True)

    class Meta:
        db_table = 'product_image'
        verbose_name = 'Product Image'
        verbose_name_plural = 'Product Images'

    def url(self):
        return os.path.join('/static/shopfront/media/images/products/', os.path.basename(str(self.product_image)))

    def photo(self):
        return mark_safe('<img src = "{}" width="80"/>'.format(self.url()))

    def __str__(self):
        return self.product_image

#--------------AddTo Wishlist Model--------------------#
class Wishlist(models.Model):
    product_name    = models.ForeignKey(ProductList, on_delete=models.DO_NOTHING, null=True) 
    sale_price      = models.IntegerField(default = 0)
    total_price     = models.IntegerField(default = 0)
    discount        = models.IntegerField(default = 0)
    quantity        = models.IntegerField(default = 1)
    created         = models.DateTimeField(auto_now_add = True)
    session_key     = models.CharField(max_length=230)

    def __str__(self):
        return str(self.product_name)
    
    class Meta:
        db_table = 'wishlist'
        verbose_name = 'Wishlist'
        verbose_name_plural = 'WishLists'
   
#--------------District Entry Model--------------------#
class DistrictInfo(models.Model):
    district_name_bangla  = models.CharField(max_length=230)
    district_name_english  = models.CharField(max_length=230)
    ordering       = models.IntegerField(default=0)
    add_date    = models.DateTimeField(auto_now_add = True)
    status         = models.BooleanField(default=True)
    
    def __str__(self):
        return str(self.district_name_english)

    class Meta:
        db_table = 'district_list'

#--------------Upozilla Entry Model--------------------#
class UpozillaInfo(models.Model):
    district_name  = models.ForeignKey(DistrictInfo, on_delete=models.DO_NOTHING, null=True)
    upozilla_name_bangla   = models.CharField(max_length=230)
    upozilla_name_english  = models.CharField(max_length=230)
    add_date       = models.DateTimeField(auto_now_add = True)
    status         = models.BooleanField(default=1) # type: ignore

    def __str__(self):
        return str(self.upozilla_name_english)
        
    class Meta:
        db_table = 'upozilla_list'

#--------------Post Office Info Model--------------------# 
class PostOfficeInfo(models.Model):
    upozilla_name  = models.ForeignKey(UpozillaInfo, on_delete=models.DO_NOTHING, null=True)
    post_office_bangla      = models.CharField(max_length=230)
    post_office_english     = models.CharField(max_length=230)
    post_code      = models.CharField(max_length=230)
    add_date       = models.DateTimeField(auto_now_add = True)
    status         = models.BooleanField(default=1) # type: ignore

    def __str__(self):
        return str(self.post_office_bangla)
        
    class Meta:
        db_table = 'post_office_list'

#--------------User Registration Model --------------------# 
class UserRegistration(models.Model):
    first_name  = models.CharField(max_length=65, blank = True)
    last_name   = models.CharField(max_length=65, blank = True)
    full_name   = models.CharField(max_length=130, blank = True)
    email       = models.CharField(max_length=130, blank = True)
    mobile      = models.CharField(max_length=50, blank = True)
    mobile2     = models.CharField(max_length=50, blank = True)
    designation = models.CharField(max_length=60, blank = True)
    photo       = models.FileField(upload_to='images/user_progile', blank = True)    
    select_type = (
        ('1', 'Admin Dashboard'),
        ('2', 'Collector Dashboard'),
        ('3', 'Delivery Dashboard'),
    )
    user_type   = models.CharField(max_length=2, choices = select_type)
    username    = models.CharField(max_length=65 )
    password    = models.CharField(max_length=65 )
    create_date = models.DateTimeField(auto_now_add = True)
    status      = models.BooleanField(default=True)
    
    def __str__(self):
        return self.full_name

    class Meta:
        db_table = 'user_list'
        verbose_name = 'User'
        verbose_name_plural = 'User List'

#--------------Menu List Models --------------------# 
class MenuList(models.Model):
    menu_name   = models.CharField(max_length=50)
    menu_url    = models.CharField(max_length=90)
    module_types = (
        ('HR', 'HR'),
        ('Attendance', 'Attendance'),
        ('Leave', 'Leave'),
        ('Payroll', 'Payroll'), 
        ('Ecommerce', 'Ecommerce'), 
    )
    module_name      = models.CharField(max_length=50, choices=module_types)
    module_order     = models.IntegerField(default=0)
    menu_order       = models.IntegerField(default=0)
    menu_photo       = models.ImageField(upload_to='images/menu_images', blank = True)  
    background_color = models.CharField(max_length=30,blank=True)
    font_color       = models.CharField(max_length=30,blank=True)
    is_dashboard     = models.BooleanField(default=False)
    is_root          = models.CharField(max_length=50, choices=module_types)
    parent_id        = models.CharField(max_length=50, choices=module_types)
    created_by       = models.ForeignKey(User, on_delete = models.DO_NOTHING, blank=True, null=True)
    modified_by      = models.IntegerField(blank=True, null=True)
    created          = models.DateTimeField(auto_now_add=True, null=True)
    modified         = models.DateTimeField(auto_now=True, null=True)
    status           = models.BooleanField(default=True)

    def __str__(self):
        return self.menu_name

    class Meta:
        db_table = 'menu_list'
        verbose_name = "Menu"
        verbose_name_plural = "Menu List"  

#--------------User Access Control Models --------------------# 
class UserAccessControl(models.Model):
    user            = models.ForeignKey(UserRegistration, on_delete = models.DO_NOTHING, null=True) 
    menu            = models.ForeignKey(MenuList, on_delete = models.DO_NOTHING, null=True)
    view_action     = models.BooleanField(default=0)      # type: ignore
    insert_action   = models.BooleanField(default=0)      # type: ignore
    update_action   = models.BooleanField(default=0)      # type: ignore
    delete_action   = models.BooleanField(default=0)      # type: ignore
    permission_date = models.DateTimeField(auto_now_add=True)
    permitted_by    = models.BigIntegerField(default=0)
    insert_date     = models.DateTimeField(auto_now_add=True)
    last_update     = models.DateTimeField(auto_now=True)
    insert_by       = models.IntegerField(default=0)
    update_by       = models.IntegerField(default=0) 
    status          = models.BooleanField(default=True)   
    
    def __str__(self):
        return str(self.user)   
        
    class Meta:
        db_table = 'user_access_control'
        verbose_name = "Access Control"
        verbose_name_plural = "User Access Control"                
 
#--------------Customar Account Models --------------------# 
class Customars(models.Model):
    customer_name   = models.CharField(max_length=30)
    mobile          = models.CharField(max_length=15)
    optional_mobile = models.CharField(max_length=15, blank = True)
    email           = models.CharField(max_length=170, blank=True, null = True)
    user_name       = models.CharField(max_length=100, blank=True)
    password        = models.CharField(max_length=100)
    login_otp       = models.IntegerField(blank=True, null = True)
    reg_date        = models.DateTimeField(auto_now_add=True)
    address         = models.TextField(blank=True)
    profile_images  = models.ImageField(upload_to='images/customer_images', blank = True)  
    is_guest        = models.BooleanField(default=1) # type: ignore
    customer_level  = models.CharField(max_length=55, blank=True, null=True) 
    total_point     = models.DecimalField(max_digits=30, decimal_places=6, default=0) # type: ignore
    used_point      = models.DecimalField(max_digits=30, decimal_places=6, default=0) # type: ignore
    available_point = models.DecimalField(max_digits=30, decimal_places=6, default=0) # type: ignore
    created         = models.DateTimeField(auto_now_add=True, null=True)
    status          = models.BooleanField(default=1) # type: ignore

    def __str__(self):
        return self.customer_name

    class Meta:
        db_table = 'customer_list'
 
#--------------Courier Service Models --------------------# 
class CourierService(models.Model): 
    courier_name    = models.CharField(max_length=60, blank=True, null = True)
    courier_address = models.TextField(blank=True, null = True)
    ordering        = models.IntegerField(default=0)
    status          = models.BooleanField(default=1)  # type: ignore

    def __str__(self):
        return self.courier_name

    class Meta:
        db_table = 'courier_services'

#--------------Sales Order Models --------------------# 
class SalesOrder(models.Model): 
    order_number        = models.IntegerField(default=0)
    customer_name       = models.ForeignKey(Customars, on_delete=models.DO_NOTHING, blank = True, null=True) 
    delivery_per_name   = models.CharField(max_length=180)
    customer_mobile     = models.CharField(max_length=15)
    optional_number     = models.CharField(max_length=15, blank = True)
    customer_email      = models.EmailField(max_length=150, blank = True)
    shipping_address    = models.CharField(max_length=250) 
    total_amount        = models.IntegerField(default=0)
    vat_amount          = models.IntegerField(default=0)
    discount_amount     = models.IntegerField(default=0)
    less_amount         = models.IntegerField(default=0)
    due_amount          = models.IntegerField(default=0) 
    shipping_charge     = models.IntegerField(default=0)
    service_charge      = models.IntegerField(default=0)
    service_code        = models.IntegerField(default=0)
    grand_total         = models.IntegerField(default=0)
    payment_type_choose = (
        ('1', 'COD'),
        ('2', 'Bkash'),
        ('3', 'Rocket'),
        ('4', 'Nagad'),
    )
    payment_method  = models.CharField(max_length=1, choices=payment_type_choose, default=1)  # type: ignore
    delivery_method = models.ForeignKey(CourierService, on_delete=models.DO_NOTHING, blank = True, null=True)
    payment_status_choose = (
        ('1', 'Pending'),
        ('2', 'Unpaid'),
        ('3', 'Paid'),
        ('4', 'Refund'),
    )
    payment_status  = models.CharField(max_length=1, choices=payment_status_choose, default=1) # type: ignore
    order_status_choose = (
        ('1', 'Pending'),
        ('2', 'Confirmed'),
        ('3', 'Packed'),
        ('4', 'Shipping'),
        ('5', 'Delivered'),
        ('6', 'Returned'),
        ('7', 'Canceled'),
        ('8', 'Hold'),
    )
    order_status        = models.CharField(max_length=1, choices=order_status_choose, default=1) # type: ignore
    district            = models.ForeignKey(DistrictInfo, on_delete=models.DO_NOTHING, null=True, blank=True)
    upozilla            = models.ForeignKey(UpozillaInfo, on_delete=models.DO_NOTHING, null=True, blank=True)
    postal_code         = models.ForeignKey(PostOfficeInfo, on_delete=models.DO_NOTHING, null=True, blank=True)
    session_key      = models.CharField(max_length=230)
    order_date       = models.DateTimeField(auto_now_add=True)
    update_date      = models.DateTimeField(auto_now_add=False, null = True)
    order_remarks    = models.TextField(blank=True,null=True)
    status           = models.BooleanField(default=1) # type: ignore

    def __int__(self):
        return self.order_number

    class Meta:
        db_table = 'sales_order'
        verbose_name = 'Sales Order'
        verbose_name_plural = 'Sales Orders'

#--------------Sales Details Models --------------------# 
class SalesDetails(models.Model):
    order_number    = models.IntegerField(default=0)  
    product_name    = models.ForeignKey(ProductList, on_delete=models.DO_NOTHING, blank = True, null=True) 
    unit_price      = models.IntegerField(default=0)
    sale_price      = models.IntegerField(default=0)
    quantity        = models.IntegerField(default=1)
    order_date      = models.DateTimeField(auto_now_add=True)
    update_date     = models.DateTimeField(auto_now_add=False, blank = True, null=True) 
    session_key     = models.CharField(max_length=230, blank = True, null=True) 
    remarks         = models.TextField(blank = True)
    status          = models.BooleanField(default=1)  # type: ignore
    insert_by       = models.IntegerField(default=0)
    update_by       = models.IntegerField(default=0)

    def __int__(self):
        return self.order_number

    class Meta:
        db_table = 'sales_details'
        verbose_name = 'Sales Detail'
        verbose_name_plural = 'Sales Details'
 
#--------------Sales Order Payment Details Models --------------------# 
class SalesOrderPaymentDetails(models.Model):
    sales_order       = models.ForeignKey(SalesOrder, on_delete=models.DO_NOTHING, null=True, blank=True)
    order_number      = models.IntegerField(default=0) 
    payment_type_choose = (
        ('1', 'COD'),
        ('2', 'Bkash'),
        ('3', 'Rocket'),
        ('4', 'Nagad'),
    )
    payment_method  = models.CharField(max_length=1, choices=payment_type_choose, default=1) # type: ignore
    payment_status_choose= (
        ('1', 'Partial'),
        ('2', 'Pull'),
        ('3', 'Due'),
        ('4', 'Free'),
    )
    payment_status  = models.CharField(max_length=1, choices=payment_status_choose, default=1) # type: ignore
    account_number      = models.IntegerField(default=0)
    payment_amount      = models.IntegerField(default=0)
    txt_number          = models.CharField(max_length=50,blank=True,null=True)
    create_date     = models.DateTimeField(auto_now_add = True)
    update_date     = models.DateTimeField(auto_now_add = False)
    status          = models.BooleanField(default=True)
    insert_by       = models.IntegerField(default=0)
    update_by       = models.IntegerField(default=0)
    
    def __str__(self):
        return self.order_number

    class Meta:
        db_table = 'sales_order_payment_details'
        verbose_name = 'Sales Order Payment Detail'
        verbose_name_plural = 'Sales Order Payment Details'

#--------------Product Offer Models --------------------#  
class ProductOffers(models.Model):
    offer_title    = models.CharField(max_length=230)
    link           = models.TextField(blank=True)
    offer_details  = models.TextField(blank=True)
    create_date    = models.DateTimeField(auto_now_add = True)
    insert_by      = models.ForeignKey(Customars, on_delete=models.DO_NOTHING, null=True, blank=True)
    status         = models.BooleanField(default=False)
    
    def __str__(self):
        return self.offer_title

    class Meta:
        db_table = 'product_offers'
        verbose_name = 'Product Offer'
        verbose_name_plural = 'Product Offers'

#--------------Client Review Models --------------------#  
class ClientReview(models.Model):
    product_name    = models.ForeignKey(ProductList, on_delete=models.DO_NOTHING, null=True, blank=True)
    client_name     = models.ForeignKey(Customars, on_delete=models.DO_NOTHING, null=True, blank=True)
    message         = models.TextField(blank = True)
    rating_count    = models.IntegerField(default = 0)
    total_like      = models.IntegerField(default = 0)
    total_dislike   = models.IntegerField(default = 0) 
    create_date     = models.DateTimeField(auto_now_add = True)
    status          = models.BooleanField(default=True)
    
    def __str__(self):
        return self.client_name

    class Meta:
        db_table = 'client_reviews'
        verbose_name = 'Client Review'
        verbose_name_plural = 'Client Reviews'
   

#-------------- Blog Models --------------------#  
class Blog(models.Model): 
    blog_title          = models.CharField(max_length=256, blank=True, null=True) 
    blog_title_english  = models.CharField(max_length=256, blank=True, null=True)
    blog_url            = models.CharField(max_length=256, blank=True, null=True)
    category            = models.ForeignKey(Category, on_delete=models.DO_NOTHING, blank=True, null=True)
    blog_image          = models.ImageField(upload_to='images/blog_image', blank=True, null=True)
    blog_details        = RichTextField(blank = True, null=True)
    meta_keyword        = models.TextField(blank = True, null=True)
    meta_description    = models.TextField(blank = True, null=True)
    create_date         = models.DateTimeField(auto_now_add = True)
    created_by          = models.ForeignKey(UserRegistration, on_delete=models.DO_NOTHING, blank=True, null=True)
    total_like          = models.IntegerField(default=0) 
    total_view          = models.IntegerField(default=0) 
    total_comments      = models.IntegerField(default=0) 
    status              = models.BooleanField(default=True)  

    def __str__(self):
        return self.blog_title

    class Meta:
        db_table = 'blog_list'
        verbose_name = "Blog"
        verbose_name_plural = "Blog List"

#-------------- Blog Comment Models --------------------#  
class BlogComment(models.Model): 
    blog            = models.ForeignKey(Blog, on_delete=models.DO_NOTHING, blank=True, null=True)  
    user_name       = models.CharField(max_length=60, blank=True, null = True)
    user_mobile     = models.CharField(max_length=60, blank=True, null = True)
    user_email      = models.EmailField(max_length=60, blank=True, null = True) 
    user_comments   = RichTextField(blank = True, null=True)
    create_date     = models.DateTimeField(auto_now_add = True) 
    status          = models.BooleanField(default=True) 

    def __str__(self):
        return self.user_name

    class Meta:
        db_table = 'blog_comments'
        verbose_name = "Blog Comment"
        verbose_name_plural = "Blog Comments"

 